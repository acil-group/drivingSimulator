#Author: Niklas Melton
#Date: 04/04/2016
#Model trainer for pyDrive

import pyDrive, pygame, sys, time, math, numpy as np
from NeuralNets import MLP, HDP
from pygame.locals import *

screenSize = 1000, 600


carSize = 40,20
senseNum = 15
speed = 1.0
steerRange = math.radians(60)
sRange = 100

my_course = pyDrive.course('kidney.txt')
startPos = my_course.startPos
my_vehicle = pyDrive.car(carSize, startPos)
my_env = pyDrive.env(screenSize, my_vehicle, my_course, senseNum, sRange)
my_env.update(1,0)

#A = MLP.loadNetwork('SLactor.txt')
model = MLP.network((16,20,20,15))
#model = MLP.loadNetwork('model.txt')
LR = 0.2
MR = 0.1

avgError = 1

#Tells the number of joysticks/error detection
joystick_count = pygame.joystick.get_count()
print ("There is ", joystick_count, "joystick/s")
if joystick_count == 0:
       print ("Error, I did not find any joysticks")
else:
       print('Joystick found')
       my_joystick = pygame.joystick.Joystick(0)
       my_joystick.init()
go = 0
print('Press A to play/pause')
while go != 1:
    
    for event in pygame.event.get():
         if event.type == QUIT:
              pygame.quit()
              sys.exit()
    go = my_joystick.get_button(0)
time.sleep(1)
 
while True:

    t = 0
    i = 0
    
    Xs = my_env.getSense()/sRange
    #s = A.feedForward(Xs) 


    while my_env.crashed == False:
        t += 1
        if t > 10000:
             t = 0
             i += 1
        if i > 50:
             model.saveNetwork('model11.txt')
        for event in pygame.event.get():
             if event.type == QUIT:
                  pygame.quit()
                  sys.exit()
       
        go = my_joystick.get_button(0)
        if go == 1:
             print('Press A to play/pause')
             model.saveNetwork('model11.txt')
             time.sleep(1)
             go = 0
             while go != 1:
                  for event in pygame.event.get():
                       if event.type == QUIT:
                            pygame.quit()
                            sys.exit()
                  go = my_joystick.get_button(0)
             time.sleep(1)
        
        s = MLP.norm(my_joystick.get_axis(0),-1,1)
        steer = MLP.anorm(s,-steerRange/2,steerRange/2)
        Xsa = np.copy(Xs)
        Xs = np.append(Xs, s)
        
        #print(np.round(Xs,2))
        modelPos = model.feedForward(Xs)
        #print(np.round(modelPos,2))
        my_env.update(speed,steer)
        Xs = my_env.getSense()/sRange
        #print(np.round(abs(Xs-modelPos),4))
        #print(HDP.qualPrint(Xs-Xsa))
        #print(HDP.qualPrint(modelPos-Xsa))
        modelError = MLP.BPerror(modelPos, Xs)
        model.backProp(modelError, LR, MR)
        error = np.sum(abs(modelPos-Xs))/len(modelError)
        avgError = (avgError*1000+error)/1001
        print(error,avgError)
    print('------crashed------\n')
    print('Press A to play again')
    time.sleep(1)
    go = 1
    while go != 1:
         for event in pygame.event.get():
              if event.type == QUIT:
                   pygame.quit()
                   sys.exit()
         go = my_joystick.get_button(0)
    time.sleep(1)
    my_env.reset()
