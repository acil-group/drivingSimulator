#Author: Niklas Melton
#Date: 02/04/2016
#pyDrive basic test code

import pyDrive, pygame, sys, time, math
from pygame.locals import *

print('INPUT REQUIRED')

courseChoice = input('Use a course? (y/n)')
if courseChoice == 'y':
       my_course = pyDrive.course('kidney.txt')
       screenSize = my_course.screenSize
       #startPos automatically found from course start line
       startPos = my_course.startPos
else:
       my_course = None
       screenSize = (1000,600)
       #startPos = [X,Y,theta,theta_trailer]
       startPos = [500,300, 0, math.pi]
print('Vehicle Options:')
print('1. Car')
print('2. Bot')
print('3. Tractor-Trailer')
vehChoice = int(input('Select a vehicle:'))
if vehChoice == 1:
       #size is (L,W)
       carSize = (40,20)
       my_vehicle = pyDrive.car(carSize, startPos)
       steerRange = math.radians(60)
elif vehChoice == 2:
       #size is (L,W)
       botSize = (30,30)
       my_vehicle = pyDrive.bot(botSize, startPos)
       #bot steering is relative to each wheel's speed
       steerRange = 1
else:
       #size is ((L_cab,W_cab),(L_trailer, W_trailer))
       truckSize = ((30,20),(60,20))
       my_vehicle = pyDrive.truck(truckSize, startPos)
       steerRange = math.radians(60)

senseChoice = input('Sensors? (y,n)')
if senseChoice == 'y':
       senseNum = int(input('How many sensors?'))
       sRange = 100
else:
       senseNum = 0
       sRange = 0

my_env = pyDrive.env(screenSize, my_vehicle, my_course, senseNum, sRange)

print('Controller options')
print('1. xBox controller')
print('2. Keyboard')
controlChoice = int(input('Controller Choice:'))
if controlChoice == 1:
       #Tells the number of joysticks/error detection
       joystick_count = pygame.joystick.get_count()
       print ("There is ", joystick_count, "joystick/s")
       if joystick_count == 0:
              print ("Error, I did not find any joysticks")
       else:
              print('Joystick found')
              my_joystick = pygame.joystick.Joystick(0)
              my_joystick.init()



       
speed = 0.1
steer = 0



go = 0
print('Press A to play')
 
while True:
       while go != 1:
              for event in pygame.event.get():
                     if event.type == QUIT:
                            pygame.quit()
                            sys.exit()
                     if event.type == pygame.KEYDOWN:
                            if event.key == pygame.K_a:
                                   go = 1
              if controlChoice == 1:
                     if my_joystick.get_button(0) == 1:
                            go = 1
                            time.sleep(1)
                     
       while my_env.crashed == False and my_env.JK == False:
        
              for event in pygame.event.get():
                     if event.type == QUIT:
                            pygame.quit()
                            sys.exit()
                     if event .type == pygame.KEYDOWN:
                            if event.key == pygame.K_LEFT:
                                   steer -= 0.1*steerRange
                            if event.key == pygame.K_RIGHT:
                                   steer += 0.1*steerRange
                            if event.key == pygame.K_UP:
                                   speed += 0.01
                            if event.key == pygame.K_DOWN:
                                   speed -= 0.01
                            if event.key == pygame.K_a:
                                   go = 0
              if controlChoice == 1:
                     if my_joystick.get_button(0) == 1:
                            go = 0
                            print('Paused. Press A to continue')
                            
                     
              while go == 0:
                     #game paused
                     time.sleep(1)
                     for event in pygame.event.get():
                            if event.type == QUIT:
                                   pygame.quit()
                                   sys.exit()
                            if event .type == pygame.KEYDOWN:
                                   if event.key == pygame.K_a:
                                          go = 1
                     if controlChoice == 1 :
                            if my_joystick.get_button(0) == 1:
                                   go = 1
                                   time.sleep(1)
             
       
              if controlChoice == 1:
                     steer = my_joystick.get_axis(0)*steerRange-0.08
              if abs(steer) < 0.08:
                     steer = 0
              my_env.update(speed,steer, doFlip=False)
              pygame.draw.circle(my_env.screen,(0,255,0),(int(my_env.vehicle.X),int(my_env.vehicle.Y)),5)
              print(steer, speed)
              pygame.display.flip()
       print('------crashed------\n')
       print('Press A to play again')
       time.sleep(1)
       go = 0
       my_env.reset()
       time.sleep(1)

        
